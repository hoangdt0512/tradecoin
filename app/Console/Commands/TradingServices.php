<?php

namespace App\Console\Commands;

use App\Models\AccountBiget;
use App\Models\Asset;
use App\Models\User;
use bitget\api\spot\SpotAccountApi;
use bitget\ApiResponse;
use bitget\internal\BitgetRestClient;
use Illuminate\Console\Command;

class TradingServices extends Command
{
    var SpotAccountApi $spotAccountApi;
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'trading:run';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Trading';
    const margin = 0.25;
    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        $restClient = new BitgetRestClient();
        $this->spotMarketApi = $restClient->getSpotClient()->getMarketApi();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $coins = [
            [
                'coinName' => 'ETH'
            ],
            [
                'coinName' => 'BTC'
            ],
        ];
        $listCoin = [];
        foreach ($coins as $key => $coin) {
            $apiResponse = new ApiResponse($this->spotMarketApi->candles($coin['coinName'] . 'USDT_SPBL', '15min', '', '', 2));
            $data = $apiResponse->getData();
            $ratio = $this->calculator($data[0]->open, $data[1]->open);
//            if (abs($ratio) < self::margin){
//                $coins[$key] = array_merge($coin, [
//                    'ratio' => abs($ratio),
//                    'type' => $ratio >= 0 ? 'tang' : 'giam',
//                ]);mode
////                unset($coins[$key]);
//            }else{
//                $coins[$key] = array_merge($coin, [
//                    'ratio' => abs($ratio),
//                    'type' => $ratio >= 0 ? 'tang' : 'giam',
//                ]);
//                $listCoin[] = $coin['coinName'];
//            }
            $coins[$key] = array_merge($coin, [
                'ratio' => abs($ratio),
                'price' =>$data[1]->open,
                'type' => $ratio >= 0 ? 'tang' : 'giam',
            ]);
            $listCoin[] = $coin['coinName'];
        }
        $assets = Asset::query()->whereIn('coinName',$listCoin)->get();
        dd($assets,$listCoin,$coins);
        foreach ($assets as $asset){
            dd($asset,$coins);
        }
        echo 123;
    }

    private function calculator($before, $present)
    {
        return ($present - $before) * 100 / $before;
    }
}
