<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Ticker extends Model
{
    use HasFactory;
    protected $fillable = ['symbol','high24h','close','low24h','ts','baseVol','quoteVol','buyOne','sellOne','usdtVol'];
}
