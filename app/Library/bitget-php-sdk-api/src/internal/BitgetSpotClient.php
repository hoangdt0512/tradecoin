<?php


namespace bitget\internal;


use bitget\api\spot\SpotAccountApi;
use bitget\api\spot\SpotMarketApi;
use bitget\api\spot\SpotOrderApi;
use bitget\api\spot\SpotPublicApi;

class BitgetSpotClient
{
    public function getAccountApi($apiKey,$apiSecret,$passphrase){
        $bitgetApiClient = new BitgetApiClient();
        $bitgetApiClient->setApiKey($apiKey);
        $bitgetApiClient->setApiSecret($apiSecret);
        $bitgetApiClient->setPassphrase($passphrase);
        return new SpotAccountApi($bitgetApiClient);
    }

    public function getMarketApi(){
        return new SpotMarketApi(new BitgetApiClient());
    }

    public function getOrderApi(){
        return new SpotOrderApi(new BitgetApiClient());
    }

    public function getPublicApi()
    {
        return new SpotPublicApi(new BitgetApiClient());
    }

}
