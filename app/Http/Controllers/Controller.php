<?php

namespace App\Http\Controllers;

use App\Models\Currency;
use App\Models\Product;
use App\Models\Ticker;
use bitget\api\spot\SpotAccountApi;
use bitget\api\spot\SpotMarketApi;
use bitget\api\spot\SpotPublicApi;
use bitget\ApiResponse;
use bitget\internal\BitgetRestClient;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Support\Facades\DB;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    var SpotPublicApi $spotPublicApi;
    var SpotMarketApi $spotMarketApi;
    var ApiResponse $apiResponse;

    public function __construct()
    {
        $restClient = new BitgetRestClient();
        $this->spotPublicApi = $restClient->getSpotClient()->getPublicApi();
        $this->spotMarketApi = $restClient->getSpotClient()->getMarketApi();
    }

    public function syncData()
    {
        $this->syncCurrency();
        $this->syncProduct();
        $this->syncTicker();
    }

    protected function syncCurrency()
    {
        $apiResponse = new ApiResponse($this->spotPublicApi->currencies());
        $data = $apiResponse->getData();
        foreach ($data as $key => $value) {
            $value = (array)$value;
            Currency::updateOrCreate([
                'coinId'=>$value['coinId'],
                'coinName'=>$value['coinName']
            ],$value);
        }
    }

    protected function syncProduct()
    {
        $apiResponse = new ApiResponse($this->spotPublicApi->products());
        $data = $apiResponse->getData();
        foreach ($data as $key => $value) {
            $value = (array)$value;
            Product::updateOrCreate([
                'symbol'=>$value['symbol'],
            ],$value);
        }
    }

    protected function syncTicker()
    {
        $apiResponse = new ApiResponse($this->spotMarketApi->tickers());
        $data = $apiResponse->getData();
        foreach ($data as $key => $value) {
            $value = (array)$value;
            Ticker::updateOrCreate([
                'symbol'=>$value['symbol'],
            ],$value);
        }
    }
}
