<?php

namespace App\Http\Controllers;

use App\Models\AccountBiget;
use App\Models\Asset;
use bitget\api\spot\SpotAccountApi;
use bitget\api\spot\SpotMarketApi;
use bitget\api\spot\SpotPublicApi;
use bitget\ApiResponse;
use bitget\Config;
use bitget\internal\BitgetRestClient;
use Illuminate\Http\Request;

class AccountController extends Controller
{
    var SpotAccountApi $spotAccountApi;

    public function __construct()
    {
        parent::__construct();
    }

    public function test()
    {
        $this->syncAssets();
        return view('welcome');
    }

    private function syncAssets()
    {

//        dd(json_decode($this->spotMarketApi->candles('BTCUSDT_SPBL','5min','','',2)));
        $assets = Asset::all();
        dd($assets);
        foreach ($accountBigets as $accountBiget) {
            $apiKey = $accountBiget->apiKey;
            $apiSecret = $accountBiget->apiSecret;
            $passphrase = $accountBiget->passphrase;
            $restClient = new BitgetRestClient();
            $this->spotAccountApi = $restClient->getSpotClient()->getAccountApi($apiKey, $apiSecret, $passphrase);
            $apiResponse = new ApiResponse($this->spotAccountApi->assets());
            $data = $apiResponse->getData();

            foreach ($data as $key => $value) {
                $value = (array)$value;
                $value['account_id'] = $accountBiget->id;

                Asset::updateOrCreate([
                    'account_id' => $value['account_id'],
                    'coinId' => $value['coinId'],
                ], $value);
            }
        }
    }
}
