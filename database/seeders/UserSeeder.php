<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name' => 'admin',
            'email' => 'admin@123.com',
            'password' =>  bcrypt('123456'),
            'created_at'=>\Carbon\Carbon::now()->toDateTimeString()
        ]);
        DB::table('account_bigets')->insert([
            'user_id' => 1,
            'apiKey' => 'bg_2841351bee72551ca4865cfce5332dfc',
            'apiSecret' =>  'db71402d7d315338e0761c85c15f8b4a1f6d79c6d7db5d0298aa35ebeca03c96',
            'passphrase'=>'APITEST1'
        ]);
    }
}
