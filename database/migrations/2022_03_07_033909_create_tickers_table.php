<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tickers', function (Blueprint $table) {
            $table->id();
            $table->string('symbol')->comment('Symbol Id');
            $table->double('high24h')->comment('24h highest price');
            $table->double('close')->comment('Latest transaction price');
            $table->double('low24h')->comment('24h lowest price');
            $table->string('ts')->comment('System timestamp');
            $table->double('baseVol')->comment('Base coin volume');
            $table->double('quoteVol')->comment('Denomination coin volume');
            $table->double('buyOne')->comment('buy one price');
            $table->double('sellOne')->comment('sell one price');
            $table->double('usdtVol')->comment('USDT volume');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tickers');
    }
};
