<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->id();
            $table->string('symbol')->comment('Symbol ID');
            $table->string('symbolName')->comment('Symbol Name');
            $table->string('baseCoin')->comment('Base coin');
            $table->string('quoteCoin')->comment('Denomination coin');
            $table->double('minTradeAmount')->comment('Min. trading amount');
            $table->double('maxTradeAmount')->comment('Max. trading amount');
            $table->double('takerFeeRate')->comment('Taker transaction fee rate');
            $table->double('makerFeeRate')->comment('Maker transaction fee rate');
            $table->tinyInteger('priceScale')->comment('Pricing scale');
            $table->tinyInteger('quantityScale')->comment('Quantity scale');
            $table->string('status')->comment('Status');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
};
