<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('assets', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('account_id');
            $table->foreign('account_id')->references('id')->on('account_bigets');
            $table->string('coinId')->comment('Coin ID');
            $table->string('coinName')->comment('Coin name');
            $table->double('available')->comment('Available assets');
            $table->double('frozen')->comment('Frozen assets');
            $table->double('lock')->comment('Locked assets');
            $table->string('uTime')->comment('Update timing');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('assets');
    }
};
